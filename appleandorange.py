# Sam's house has an apple tree and an orange tree that yield an abundance of fruit. The apple tree is to the left of his house, and the orange tree is to its right. You can assume the trees are located on a single point, where the apple tree is at point  and the orange tree is at point.

# When a fruit falls from its tree, it lands  units of distance from its tree of origin along the -axis. A negative value of  means the fruit fell  units to the tree's left, and a positive value of  means it falls  units to the tree's right.

# Given the value of  for  apples and  oranges, can you determine how many apples and oranges will fall on Sam's house (i.e., in the inclusive range )? Print the number of apples that fall on Sam's house as your first line of output, then print the number of oranges that fall on Sam's house as your second line of output.

# s, t, house left, right
# a, b, tree position, apple, orange
# m, n, number of apples, oranges
# apples, distance for apples
# oranges, distance for oranges

s = 7
t = 11
a = 5
b = 15
m = 3
n = 2
apple = [-2, 2, 1]
orange = [5, -6]

d1 = map(lambda x: x + a, apple)
d2 = map(lambda x: x + b, orange)

q1 = map(lambda x: (x >= s) & (x <= t), d1)
q2 = map(lambda x: (x >= s) & (x <= t), d2)

print q1.count(True)
print q2.count(True)
