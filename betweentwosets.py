import random

def gcd(x,y):
    while y:
        x, y = y, x % y
    return x

def lcm(x, y):
   lcm = (x*y)/gcd(x,y)
   return lcm

a = [2, 3, 6]
b = [42, 84]

q = reduce(lcm, a)
r = reduce(gcd, b)

count  = 0
for i in range(1,r+1):
    if r % (q*i) == 0:
        count = count + 1

print count
