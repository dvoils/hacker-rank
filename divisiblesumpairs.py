import random
import math

# if a1 % k = k1, and
# if a2 % k = k2, and
# k = k1 + k2, then 
# (a1 + a2) % k = 0 

m = 2
n = 10

p = range(1,n*m)
q = range(1,n)
k = random.choice(q)

a = []
for i in range(0,n):
    a.append(random.choice(p))

#a = [1, 3, 2, 6, 1, 2]
#a = [9, 14, 6, 19, 2, 16, 17, 6, 11, 8]
#a = [15, 19, 13, 7, 13, 12, 11, 1, 11, 17]
#k = 3

def getMod(a,k):
    for i in range(0,k):
        print "************* ", i
        for j in range (0,n):
            if a[j] % k == i:
                print j

def getPairs(a,k):
    s = 0
    for i in range(0,n):
        for j in range(i+1,n):
            if (a[i] + a[j]) % k == 0:
                print i,j
                s += 1
    return s

print "k ",k
print a

s1 = getPairs(a,k)
getMod(a,k)

print "************************"

mods = [0] * k
for i in range(0,n):
    mods[a[i] % k] += 1

print mods

if k % 2 == 0:
    half = k/2
else:
    half = k/2 + 1

s = 0
for i in range(1, half):
    s += mods[i]*mods[k-i]

u = mods[0] * (mods[0] - 1)/2

if k % 2 == 0:
    v = mods[half] * (mods[half] - 1)/2
else:
    v = 0

s += u + v

print s

print s1, s, half, u, v
