n = 7
#       1  2  3  4  5  6  7
data = [4, 8, 7, 9, 3, 2, 3]
edge = [[2,4],[2,3],[3,1],[3,5],[1,7],[1,6]]

a24 = 8 + 7 + 4 + 3 + 3 + 2
b24 = 9

a23 = 8 + 9
b23 = 7 + 4 + 3 + 3 + 2

a31 = 9 + 8 + 7 + 3
b31 = 4 + 3 + 2

a35 = 9 + 8 + 7 + 4 + 3 + 2
b35 = 3

a17 = 9 + 8 + 7 + 4 + 2
b17 = 3

a16 = 9 + 8 + 7 + 4 + 3 + 3
b16 = 2

s24 = abs(a24 - b24)
s23 = abs(a23 - b23)
s31 = abs(a31 - b31)
s35 = abs(a35 - b35)
s17 = abs(a17 - b17)
s16 = abs(a16 - b16)

aSum = [a24, a23, a31, a35, a17, a16]
bSum = [b24, b23, b31, b35, b17, b16]

ans = [s24, s23, s31, s35, s17, s16]

print aSum
print bSum
print ans
