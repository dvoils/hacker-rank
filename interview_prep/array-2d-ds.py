arr1 = [
[1, 1, 1, 0, 0, 0],
[0, 1, 0, 0, 0, 0],
[1, 1, 1, 0, 0, 0],
[0, 9, 2, -4, -4, 0],
[0, 0, 0, -2, 0, 0],
[0, 0, -1, -2, -4, 0]
]

arr2 = [
[-1, -1, 0, -9, -2, -2],
[-2, -1, -6, -8, -2, -5],
[-1, -1, -1, -2, -3, -4],
[-1, -9, -2, -4, -4, -5],
[-7, -3, -3, -2, -9, -9],
[-1, -3, -1, -2, -4, -5]
]

def sampleArray(arr, r, c, n):
    A = []
    for row in range(r, r+n):
        A.append(arr[row][c:c+n])
    return A

def getSum(A):
    s = 0
    s = s + sum(A[0])
    s = s + A[1][1]
    s = s + sum(A[2])
    return s

def hourglassSum(arr):
    m = -10000
    for r in range(0,4):
        for c in range(0,4):
            S = sampleArray(arr,r,c,3)
            s = getSum(S)
            print S,s
            if s > m:
                m = s
    return m
            
print hourglassSum(arr2)
    