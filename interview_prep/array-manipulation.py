# [0,0,0,0,0]
# [100,100,0,0,0]
# [100,200,100,100,100]
#
testData = [
    [5, 3],
    [1, 2, 100],
    [2, 5, 100],
    [3, 4, 100]
]

def getTestData(A):
    a = A[0]
    arraySize = a[0]
    querySize = a[1]
    A.pop(0)
    return A, arraySize, querySize

def getSum(arr):
    l = len(arr)
    for i in range(0, l - 1):
        arr[i + 1] = arr[i] + arr[i + 1]
    return arr

def addValues(q, arr):
    a = q[0] - 1
    b = q[1]
    k = q[2]

    arr[a] = arr[a] + k
    arr[b] = arr[b] - k
    return arr

def arrayManipulation(n, queries):
    nQueries = len(queries)
    arr = [0] * (n + 1)
    for i in range(0,nQueries):
        arr = addValues(queries[i],arr)
    return max(getSum(arr))

def getData():
    f = open("queries.txt","r");
    
    f1 = f.read()
    f2 = f1.split("\n")
    
    a1 = f2[0]
    a2 = a1.split(" ")
    arraySize = int(a2[0])
    querySize = int(a2[1])
    
    f2.pop(0)
    
    A = []
    for item in f2:
        row = item.split(" ")
        R = []
        for num in row:
            R.append(int(num))
        A.append(R)
    return A, arraySize, querySize

def addValuesNaive(q, arr):
    a = q[0]
    b = q[1]
    k = q[2]
    
    for i in range(a-1,b):
        n = arr[i]
        n = n + k
        arr[i] = n
    return arr

def arrayManipulationNaive(n, queries):
    nQueries = len(queries)
    arr = [0] * n
    for i in range(0,nQueries):
        arr = addValuesNaive(queries[i],arr)
    return max(arr)

queries, arraySize, querySize = getData()
#queries, arraySize, querySize = getTestData(testData)


print "running queries..."

print arrayManipulation(arraySize, queries)
print arrayManipulationNaive(arraySize, queries)

