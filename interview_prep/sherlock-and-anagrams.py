allLetters = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z']


def isAnagram(s1, s2):
    letterCount = {}
    l = len(s1)
    
    for i in range(0,l):
        letter = s1[i]
        letterCount[letter] = 1
    
    for i in range(0,l):
        letter = s2[i]
        if letter in letterCount:
            letterCount[letter] = letterCount[letter] + 1
        else:
            letterCount[letter] = 1
    
    v = letterCount.values()
    if min(v) == 1:
        return False
    else:
        return True
    
def countAnagrams(subStrings):
    count = 0
    l = len(subStrings)
    for i in range(0,l):
        for j in range(i + 1,l):
            if isAnagram(subStrings[i], subStrings[j]):
                count = count + 1
    return count

def getSubStrings(s,n):
    l = len(s)
    subStrings = []
    for i in range(0,l - (n - 1)):
        currentWindow = s[i:i + n]
        subStrings.append(currentWindow)
    return countAnagrams(subStrings)


def sherlockAndAnagrams(s):
    count = 0
    l = len(s)
    for i in range(2,l-1):
        print i, count
        count = count + getSubStrings(s,i)
    return count

  

print sherlockAndAnagrams('jdhfifhfjvnfujfyfjfkjgfh')
#print isAnagram('hfi', 'ifn')