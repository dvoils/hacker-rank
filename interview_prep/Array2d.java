import java.util.ArrayList;
import java.util.Arrays;

public class Array2d {
  
  static int[][] A1 = {
    {-1, -1, 0, -9, -2, -2},
    {-2, -1, -6, -8, -2, -5},
    {-1, -1, -1, -2, -3, -4},
    {-1, -9, -2, -4, -4, -5},
    {-7, -3, -3, -2, -9, -9},
    {-1, -3, -1, -2, -4, -5}
  };

  static int[][] A = {
    { 0, 1, 2, 3, 4, 5 },
    { 10, 11, 12, 13, 14, 15 },
    { 20, 21, 22, 23, 24, 25 },
    { 30, 31, 32, 33, 34, 35 },
    { 40, 41, 42, 43, 44, 45 },
    { 50, 51, 52, 53, 54, 55 }
  };

  static void printArray(int arr[][]) {
    for (int i = 0; i < 3; i++) {
      for (int j = 0; j < 3; j++) {
        System.out.print(arr[i][j] + " ");
      }
      System.out.println("");
    }
  }
  
  static void sampleArray(int r, int c, int n) {

    int[][] B = new int[n][n];
    int row = r;
    
    for (int i = 0; i < n; i++) {
      B[i] = Arrays.copyOfRange(A[row], c, c + n);
      row = row + 1;
    }

    printArray(B);
  }

  public static void main(String[] args) {
    sampleArray(1,1,3);

  }
}
