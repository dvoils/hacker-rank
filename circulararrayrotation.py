import sys

# n numbers
# k rotations
# q queries

# Must use a Pythonic solution to minimize 
# time spent by the code
#
def rotate(a,k):
    return a[-k:] + a[:-k]

# Must use mod to reduces the number of rotations required
#
rn = rotate(a,k%n)

