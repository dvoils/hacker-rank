
#
class Node:
    def __init__(self, adj, value):
        self.adj = adj
        self.value = value
        self.subSum = 0

# DFS non-recursive
# 
#

# Compliment
#
def minus(q,r):
    a = set(q)
    b = set(r)
    return list(a - b)

# Union
#
def plus(q,r):
    a = q + r
    return list(set(a))

# Get remaining nodes to visit. 
#
def getrem(rem,adj,vis):
    a = plus(rem,adj)
    return minus(a,vis)
#    return minus(b,cur)



#n = 6
#data = [100, 200, 100, 500, 100, 600]
#edge = [[1,2],[2,3],[2,5],[4,5],[5,6]]

n = 7
#       1  2  3  4  5  6  7
data = [4, 8, 7, 9, 3, 2, 3]
edge = [[2,4],[2,3],[3,1],[3,5],[1,7],[1,6]]

G = {}
nodes = []
for i in range(0,len(edge)):
    nodes = plus(edge[i],nodes)

k = 0
for i in nodes:
    q = []
    for j in range(0,len(edge)):
        if i in edge[j]:
            r = minus(edge[j],[i])[0]
            q.append(r)

    G[i] = Node(q,data[k])
    k = k + 1

def DFS(s,G):
    visited = []
    remaining = [s]
    
    n = 0
    while remaining:
        current = remaining.pop()
        visited.append(current)
        remaining = getrem(remaining,G[current].adj,visited)

    return visited
    
def back_track(v,G):
    visited = list(v)
    while visited:
        back = visited.pop()
      
        if len(G[back].adj) > 1:
            children = minus(G[back].adj, visited)
            n = G[back].value
            for i in children:
                n = n + G[i].subSum
            G[back].subSum = n 
        else:
            G[back].subSum = G[back].value

visited = DFS(2,G)
back_track(visited,G)
print visited

p = 0
for q in visited:
    aSum = G[q].subSum + p

for v in visited:
    print v, G[v].adj








